#!/bin/bash

rm -f docker-root-with-portage.tar
rm -f docker-root-without-portage.tar

sudo ./large-dirs.sh
# ./large-dirs.sh

# alias dl='docker ps -l -q'

export BUILD_DIR="$PWD/build"

###
echo "Step 1: Create image \"iqhive/gentoo-patched:prepatch-with-portage\" with only portage downloaded"
###
#docker build --no-cache -f Dockerfile-prepatch-with-portage -t iqhive/gentoo-patched:prepatch-with-portage . || exit 1
docker build -f Dockerfile-prepatch-with-portage -t iqhive/gentoo-patched:prepatch-with-portage . || exit 1

###
echo "Step 2: Patch and build the image"
###
docker run --privileged -ti -v $BUILD_DIR:/build iqhive/gentoo-patched:prepatch-with-portage /build/build.sh || exit 1
IMG_PREPATCH=`docker ps -l -q`
echo IMG_PREPATCH=$IMG_PREPATCH

###
echo "Step 3: Export the newly build root fs to \"docker-root-with-portage.tar\""
###
docker export -o docker-root-with-portage.tar $IMG_PREPATCH || exit 1

###
echo "Step 4: Create the \"iqhive/gentoo-patched:with-portage\" image"
###
docker build -f Dockerfile-with-portage -t iqhive/gentoo-patched:with-portage . || exit 1
docker build -f Dockerfile-with-portage -t iqhive/gentoo-patched . || exit 1
rm  docker-root-with-portage.tar

###
echo "Step 5: Remove /usr/portage"
###
docker run --privileged -ti -v $BUILD_DIR:/build iqhive/gentoo-patched:with-portage /build/remove-portage.sh || exit 1
IMG_NOPORTAGE=`docker ps -l -q`
echo IMG_NOPORTAGE=$IMG_NOPORTAGE

###
echo "Step 6: Export the newly build root fs to \"docker-root-without-portage.tar\""
###
docker export -o docker-root-without-portage.tar $IMG_NOPORTAGE || exit 1

###
echo "Step 7: Create the \"iqhive/gentoo-patched:without-portage\" image"
###
docker build -f Dockerfile-without-portage -t iqhive/gentoo-patched:without-portage . || exit 1
rm  docker-root-without-portage.tar

exit 0

####
#echo "Step 5: Remove /usr/portage from \"docker-root-with-portage.tar\""
####
## Too slow:
##tar -f docker-root-with-portage.tar --wildcards --delete usr/portage/*
#exit 0
#cat docker-root-with-portage.tar | tar --wildcards --delete usr/portage > docker-root-without-portage.tar
#rm  docker-root-with-portage.tar
#
####
#echo "Step 6: Create the \"gentoo-patched:without-portage\" image"
####
#docker build -f Dockerfile-without-portage -t iqhive/gentoo-patched:without-portage . || exit 1

exit 0

#docker run --privileged -ti -v $PWD/build:/build iqhive/gentoo-patched:with-portage /build/build.sh
#docker export -o docker-root.tar `dl`
#docker build -f Dockerfile-latest -t $THIS_IMAGE_NAME .
# docker commit -m "My first container" `dl` iqhive/test-images

