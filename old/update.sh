#/bin/bash

export EXPORT_DIR="."
# export EXPORT_DIR="/qdata"

sudo ./large-dirs.sh
# ./large-dirs.sh

docker build -t iqhive/gentoo-patched:withportage . || exit 1
echo Starting up iqhive/gentoo-patched:withportage ...
CONTID=`docker run -d iqhive/gentoo-patched:withportage /bin/sleep 3600`
echo Container ID: $CONTID
sleep 1

echo TARring up portage...
docker exec $CONTID tar -C /usr/portage -cf /usr/portage.tar . || exit 1
echo Extracting portage from running image
docker cp $CONTID:/usr/portage.tar $EXPORT_DIR/portage.tar || exit 1
echo Removing portage.tar from running image
docker exec $CONTID rm -f /usr/portage.tar || exit 1

echo Removing portage directory from running image
docker exec $CONTID rm '-Rf' '/usr/portage' || exit 1
docker exec $CONTID mkdir '/usr/portage' || exit 1

echo Exporting root filesystem of running image
docker export -o $EXPORT_DIR/gentoo-export.tar $CONTID || exit 1

echo Stopping container
docker stop $CONTID

# echo -e "FROM scratch\nMAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>\nLABEL description=\"Base Gentoo, with latest patches\"\nADD BASE-gentoo-export.tar /" > final-image/Dockerfile
# cd final-images
#rm BASE-gentoo-export.tar
#ln -s $EXPORT_DIR/gentoo-export.tar BASE-gentoo-export.tar

#docker build -f Dockerfile-portage -t iqhive/gentoo-patched:portage .
docker build -f Dockerfile-final -t iqhive/gentoo-patched . || exit 1

# cd ..

echo Pushing images to docker hub... DISABLED 

exit 0

docker push iqhive/gentoo-patched || exit 1
docker push iqhive/gentoo-patched:withportage || exit 1

#echo Importing root FS into iqhive/gentoo-patched
#docker import $EXPORT_DIR/gentoo-export.tar iqhive/gentoo-patched --message "Gentoo amd64 latest"
#echo Importing portage.tar into iqhive/gentoo-portage
#docker import $EXPORT_DIR/portage.tar iqhive/gentoo-portage --message "Gentoo portage import"
##echo Creating portage image volume
##docker create -v /usr/portage --name iqhive-portage iqhive/gentoo-portage

# docker run --volumes-from iqhive-portage --name gentoo -it iqhive/gentoo-patched /bin/bash



# rm $EXPORT_DIR/gentoo-export.tar

# docker save -o gentoo.tar iqhive/gentoo-patched:withportage

# ./run.sh

