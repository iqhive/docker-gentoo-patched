#!/bin/bash

cp -a /build/export/. /

###############
export MAKEOPTS="-j4"
echo 'MAKEOPTS="-j4"' >> /etc/portage/make.conf
echo 'EMERGE_DEFAULT_OPTIONS="--jobs 4"' >> /etc/portage/make.conf
echo "USE=-filecaps" >> /etc/portage/make.conf
	    

#
# DEFAULT CFLAGS="-O2 -pipe"
# DEFAULT CPU_FLAGS_X86="mmx sse sse2"
#

## CPU Flags - Westmere and above
#sed -i 's/^CPU_FLAGS_X86=.*/CPU_FLAGS_X86="aes mcx16 msahf popcnt sse sse2 sse3 sse4.1 sse4.2 ssse3"/g' /etc/portage/make.conf
#sed -i 's/^CFLAGS=.*/CFLAGS="-O2 -march=westmere --param l1-cache-line-size=64 --param l1-cache-size=32 --param l2-cache-size=12288 -pipe"/g' /etc/portage/make.conf

# CPU Flags - Haswell and above
# NOT RECOMMENDED FOR PERFORMANCE (gives lower performance than Westmere and Broadwell settings)
#sed -i 's/^CPU_FLAGS_X86=.*/CPU_FLAGS_X86="aes mabm mcx16 mlzcnt msahf popcnt sse sse2 sse3 sse4.1 sse4.2 ssse3"/g' /etc/portage/make.conf
#sed -i 's/^CFLAGS=.*/CFLAGS="-O2 -march=haswell --param l1-cache-line-size=64 --param l1-cache-size=32 --param l2-cache-size=8192 -pipe"/g' /etc/portage/make.conf

## CFLAGS - Broadwell and above:
#sed -i 's/^CPU_FLAGS_X86=.*/CPU_FLAGS_X86="aes mabm mcx16 mlzcnt msahf popcnt sse sse2 sse3 sse4.1 sse4.2 ssse3"/g' /etc/portage/make.conf
#sed -i 's/^CFLAGS=.*/CFLAGS="-O2 -march=broadwell --param l1-cache-line-size=64 --param l1-cache-size=32 --param l2-cache-size=40960 -pipe"/g' /etc/portage/make.conf

# echo "dev-libs/openssl -bindist" > /etc/portage/package.use/openssl
# echo "net-misc/openssh -bindist" > /etc/portage/package.use/openssh
# emerge -v dev-libs/openssl net-misc/openssh;rm -Rf /var/tmp/portage
mount /tmp /tmp -t tmpfs
mount /var/tmp /var/tmp -t tmpfs

emerge -v --newuse --update world || exit 1

# emerge -v app-editors/jed
emerge -v net-misc/telnet-bsd net-analyzer/mtr net-misc/curl app-editors/jed || exit 1

# Net Utils
# emerge -v net-misc/iperf net-analyzer/netcat sys-apps/ethtool net-analyzer/iptraf-ng; rm -Rf /var/tmp/portage

sed -i 's/^MAKEOPTS=.*/MAKEOPTS="-j4"/g' /etc/portage/make.conf; sed -i 's/^EMERGE_DEFAULT_OPTIONS=.*/EMERGE_DEFAULT_OPTIONS="--jobs 4"/g' /etc/portage/make.conf

# rm -Rf /var/tmp/portage

cp /usr/share/zoneinfo/GMT /etc/localtime 

# rm -Rf /usr/portage/*
# eselect profile list
# eselect profile set xxx

rm -Rf /var/tmp/portage

